# WinToUsbLinux
Create a Windows USB install stick using Linux machine.
## Usage
### Install dependencies
```sh
$ sudo apt-get install wimtools parted rsync
```

### Run the script (as root, or sudo)
```sh
$ sudo chmod +x WinToUsbLinux.sh
$ sudo ./WinToUsbLinux.sh [ISO IMAGE PATH]

# for example:
# $ sudo ./WinToUsbLinux.sh /home/vaared/Downloads/Win10_1909_English_x64.iso
```
##### Download Windows 10 ISO image:
https://www.microsoft.com/en-US/software-download/windows10ISO

## Thanks for
* [@ypcs](https://twitter.com/ypcs) - Ville Korhonen. [Create Windows 10 UEFI install USB from ISO What were they thinking?](https://ypcs.fi/howto/2018/12/01/windowsiso/)
* [WoeUSB](https://github.com/slacka/WoeUSB/blob/master/src/data/listUsb)'s listUsb 

## License
WTFPL
